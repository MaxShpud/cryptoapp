package com.example.domain.models

data class UserNameParam(val name: String)