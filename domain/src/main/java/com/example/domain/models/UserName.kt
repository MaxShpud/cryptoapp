package com.example.domain.models

data class UserName(
    val firstName: String,
    val secondName: String
)
