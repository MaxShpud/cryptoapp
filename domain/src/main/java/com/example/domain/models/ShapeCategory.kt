package com.example.domain.models

data class ShapeCategory(
	val name: String,
	val isEmpty: Boolean,
	val count: Int
)